# MongoDB <a name="inicio"/>

## Contenido
* 1. [Configuraciones](#Configuraciones)
* 2. [Comandos básicos](#Comandos)
    * 1. [Base de datos](#DB)
    * 2. [Colecciones](#Colecciones)
    * 3. [Documentos](#Documentos)
      * 1. [IMPORT](#IMPORT)
      * 2. [CREATE](#CREATE)
      * 3. [READ](#READ)
      * 4. [UPDATE](#UPDATE)
      * 5. [DELETE](#DELETE)


---
## Configuraciones <a name="Configuraciones"/> [↑](#inicio)
### Correr MongoDb
Ejecutar el archivo 'mongod.exe' de 'C:\Program Files\MongoDB\Server\4.2\bin'

### Abrir Interprete de Comandos
Desde consola, ubicado en la dirección 'C:\Program Files\MongoDB\Server\4.2\bin', ejecutar el comando `"C:\Program Files\MongoDB\Server\4.2\bin\mongo.exe"`

### Crear Respaldo de Base de datos
Desde la consola de MongoDB, o desde el shell, ubicado en la dirección 'C:\Program Files\MongoDB\Server\4.2\bin', ejecutar los siguientes comandos:
* `mongodump --db "nombreDbARespaldar"`
* `mongorestore --db NombreDeNuevaDb "pathDeDbARestaurar/nombreDb"`

EJEMPLO:
* `mongodump --db banco`
* `mongorestore --db bancoDelete "dump\banco"`

**Comando para MongoDb 4.0 y anteriores:** `db.copyDatabase("BaseDeDatosACopiar", "NombreDeLaCopia", "127.0.0.1")`


---
## Comandos básicos <a name="Comandos"/> [↑](#inicio)
### Base de datos <a name="DB"/> [↑](#inicio)
Comando | Descripción
--------|------------
`db` | Imprime la Db en la que te encuentras.
`show dbs` | Muestra las bases de datos.
`use nameDB` | Crea o cambia a la Db especificada.
`db.dropDatabase()` | Elimina la Db.

### Colecciones <a name="Colecciones"/> [↑](#inicio)
Comando | Descripción
--------|------------
`show collections` | Muestra las colecciones.
`db.createCollection("Name")` | Crea una colección.
`db.collectionName.drop()` | Elimina la colección.

### Documentos <a name="Documentos"/> [↑](#inicio)
#### IMPORT <a name="IMPORT"/> [↑](#inicio)
Comando | Descripción
--------|------------
`mongoimport --jsonArray --db nombreBaseDeDatos --collection nombreColección --file /path/delArchivoTipoJSON` | Importa queries de tipo json en una colección de una base de datos MongoDb.

#### CREATE <a name="CREATE"/> [↑](#inicio)
<table>
  <tr>
    <th>Comando</th>
    <th>Descripción</th>
  </tr>
  <tr>
    <td>
      <pre><code>
        db.collectionName.insert({
          "clave": "Valor",
          "clave": 4,
          "clave": null,
          "clave": ["Valor1", "Valor2", "Valor3"], 
          "clave": {
            "clave": false,
            "clave": true
          }
        })
      </code></pre>
    </td>
    <td> Crea uno o varios documentos en la colección, depende de la sintaxis 
    </td>
  </tr>
  <tr>
    <td>
      <pre><code>
        db.collectionName.insertOne(
            "clave": "Valor",
            "clave": 4,
            "clave": true
        })
      </code></pre>
    </td>
    <td> Crea un documento en la colección 
    </td>
  </tr>  
  <tr>
    <td>
      <pre><code>
        db.collectionName.insertMany([{
          "clave": "Valor",
          "clave": "Valor",
        }, {
          "clave": "Valor",
          "clave": "Valor",
        }, {
          "clave": "Valor",
          "clave": "Valor",
        }])
      </code></pre>
    </td>
    <td> Crea varios documentos en la colección 
    </td>
  </tr>
  <tr>
    <td>
      <pre><code>
        db.collectionName.insert([{},{}])
      </code></pre>
    </td>
    <td> Crea un arreglo de documentos 
    </td>
  </tr>
</table>

#### READ <a name="READ"/> [↑](#inicio)
Comando | Descripción
--------|------------
`db.collectionName.find() ` | Muestra los documentos de la colección
`db.collectionName.find().pretty() ` | Muestra los documentos de la colección de manera ordenada (en formato BSON)
`db.collectionName.find().count()` | Muestra la cantidad de documentos

##### Operadores '$eq', '$ne', '$gt', '$gte', '$lt' y '$lte'
Operdador | Comando | Descripción
----------|---------|------------
`$eq` | `db.collectionName.find({"campo": {"$eq": "valor"}})` | Muestra los documentos que contengan el campo IGUAL (Equal) al valor que se ingreso.
`$ne` | `db.collectionName.find({"campo": {"$ne": "valor"}})` | Muestra los documentos que contengan el campo DIFERENTE (No Equal) al valor que se ingreso.
`$gt` | `db.collectionName.find({"campo": {"$gt": "valor"}})` | Muestra los documentos que contengan el campo MAYOR A (Greater Than) al valor que se ingreso.
`$gte` | `db.collectionName.find({"campo": {"$gte": "valor"}})` | Muestra los documentos que contengan el campo MAYOR A O IGUAL (Greater Than or Equal) al valor que se ingreso.
`$lt` | `db.collectionName.find({"campo": {"$lt": "valor"}})` | Muestra los documentos que contengan el campo MENOR A (Less Than) al valor que se ingreso.
`$lte` | `db.collectionName.find({"campo": {"$lte": "valor"}})` | Muestra los documentos que contengan el campo MENOR A O IGUAL (Less Than or Equal) al valor que se ingreso.
Más de un Operador | `db.collectionName.find({"campo": {"$lt": "valor", "$gt": "valor"}})` | Utilizando más de un operador.
`sort` | `db.collectionName.find({"campo": {"$eq": "valor"}}).sort({"campo": 1})` | Documentos ORDENADOS DE MAYOR A MENOR.
`sort` | `db.collectionName.find({"campo": {"$eq": "valor"}}).sort({"campo": -1})` | Documentos ORDENADOS  DE MENOR A MAYOR.
Objeto | `db.collectionName.find({"objeto.campo": {"$eq": "valor"}})` | Utilizando el campo de un OBJETO IGUAL.
Array | `db.collectionName.find({"array": "valor})` | Muestra los documentos que contengan el campo especificado en el array especificado.
Array | `db.collectionName.find({"array.numeroDePosición": "valor})` | Muestra los documentos que contengan el campo en la posición del array especificados.

##### Operadores '$in' y '$nin'
Operdador | Comando | Descripción
----------|---------|------------
`$in` | `db.collectionName.find({"objeto.campo": {"$in": ["valor1", "valor2"]}})` | Busca coincidencias con cualquiera de los valores del array especificado.
`$nin` | `db.collectionName.find({"objeto.campo": {"$nin": ["valor1", "valor2"]}})` | Busca que no haya coincidencias con ninguno de los valores del array especificado.

##### Operadores '$and' y '$or'
Operdador | Comando | Descripción
----------|---------|------------
`$and` | `db.collectionName.find({"$and": [{"campo": {"$ne": "valor1"}, {"campo": {"$gte": "valor2"}}]})` | Selecciona los documentos que cumplen TODAS las expresiones (condiciones) del array.
`$or` | `db.collectionName.find({"$or": [{"campo": {"$ne": "valor1"}, {"campo": {"$gte": "valor2"}}]})` | Selecciona los documentos que cumplen AL MENOS UNA de las expresiones (condiciones) del array.

#### UPDATE <a name="UPDATE"/> [↑](#inicio)
Operador | Comando | Descripción
---------|---------|------------
`$set` | `db.collectionName.update({"_idDocumento": "valor"}, {"$set": {"campoAModificar1": "valorAModificar1"}, {"campoAModificar2": "valorAModificar2"}})` | Remplaza los valores de los campos de un documento por el valor especificado en el comando.
`$set` | `db.collectionName.update({"_idDocumento": "valor"}, {"$set": {"campoAAgregar": "valorAAgregar"}})` | Agrega un campo al documento con el valor especificado en el comando
`$set` | `db.collectionName.update({"$set": {"campoAModificar1": "valorAModificar1"}, {"campoAModificar2": "valorAModificar2"}})` | Remplaza los valores de los campos del primer documento encontrado por el valor especificado en el comando.
`$set` | `db.collectionName.update({}, {$set: {"campo": "valor"}}, {multi: true})` | Actualiza multiples documentos.
`$set` | `db.collectionName.update({"_idDocumento": "valor"}, {$set: {"createAt": new Date()}})` o `db.collectionName.updateOne({"__idDocumento": "valor"}, {$currentDate: {"createAt": true}})` | Crea un campo con una fecha actual (se pude hacer con dos comandos).
`$set` | `db.collectionName.update({"__idDocumento": "valor", "arreglo": "valorDelElementoAModificar"}, {$set: {"arreglo.$": "nuevoValor"}})` | Cambia el valor de un elemento de un array especificado.
`$set` | `db.collecctionName.update({"__idDocumento": "valor", "arreglo.idObjeto": "valor"}, {$set: {"arreglo.$.campo": "nuevoValor"}})` | Cambia el valor de un campo de un objeto que forma parte de los elementos de un array de objetos. **NOTA:** Si se agrega un campo que no existe en el objeto entonces se crea el campo con ese nombre.
`$unset` | `db.collectionName.update({"_idDocumento": "valor"}, {"$unset": {"campoAEliminar": "valorAEliminar"}})` o `db.collectionName.update({"_idDocumento": "valor"}, {"$unset": {"campoAEliminar": 1}})` | Borra un campo particular de un documento (dos comandos posibles).
`$unset` | `db.collectionName.update({"__idDocumento": "valor", "arreglo": "valorDelElementoAModificar"}, {$unset: {"arreglo.$": 1}})` | Cambia el valor a null de un elemento de un array especificado.
Más de un Operador | `db.collectionName.updateOne({"idDocumentoARemplazar": "valor"}, {$set: {"campo": "nuevoValor"}, $unset: {"campoAEliminar": 1}})` | Utilizando multiples operadores ($set y $unset).
`$updateOne` | `db.collectionName.updateOne({"campoId": "valorId"}, {$set: {"campoAModificar": "valorAModificar}})` | Actualiza el valor del campo de un documento especificado.
`$updateMany` | `db.collectionName.updateMany({}, {$rename: {"campoAActualizar": "nuevoCampo"}})` | Renombra un campo en todos los documentos de la colección.
`$updateMany` | `db.collectionName.updateMany({"campoId": "valorId"}, {$set: {"campoAModi	ficar": "valorAModificar}})` | Actualiza el valor del campo de los documentos especificados.
`$push` | `db.collectionName.update({"_idDocumento": "valor"}, {"$push": {"arreglo": "nuevoValor"}})` | Agrega un campo a un arreglo de un documento. *
`$each` | `db.collectionName.update({"_idDocumento": "valor"}, {"$push": {"arreglo": $each{ ["nuevoValor1", "nuevoValor2", "nuevoValor3"]}}})` | Agrega varios campos a un arreglo de un documento. *
`$addToSet` | `db.collectionName.update({"_idDocumento": "valor"}, {"$addToSet": {"arreglo": $each{ ["nuevoValor1", "nuevoValor2", "nuevoValor3"]}}})` | Agrega un campo único a un arreglo de un documento, si el campo ya existe en el arreglo entonces no lo crea. *
`$pop` | `db.collectionName.update({"_idDocumento": "valor"}, {"$pop": {"arreglo": 1}})` | Elimina un campo (elemento) de un arreglo de un documento. **NOTA:** Si se agrega un "1" se elimina el último elemento del array, si se agrega un "-1" se elimina el primer elemento.
`$pull` | `db.collectionName.update({"_idDocumento": "valor"}, {"$pull": {"arreglo": {$gte: NumDePosición}}})` | Elimina (un) campo(s) (elemento(s)) especificado(s) de un arreglo de un documento. **NOTA:** En la parte del operador $gte puede ser cualquiera de los operadores: `$eq`, `$ne`, `$gt`, `$gte`, `$lt` y `$lte`.
`$pullAll` | `db.collectionName.update({"_idDocumento": "valor"}, {"$pull": {"arreglo": "valor"}})` | Elimina todos los elementos del valor que se le pasa al arreglo del documento especificado.
`$elemMatch` | `db.collecctionName.update({"__idDocumento": "valor", "arreglo": {$elemMatch: {"campo1": "valor", "campo2": "valor"}}}, {$set: {"arreglo.$.nuevoCampo": "nuevoValor"}})` | Agrega un campo a un objeto de un arreglo de objetos. El comando $elemMatch permite identificar un objeto con los campos que conocemos.
`$inc` | `db.collecctionName.update({"__idDocumento": "valor", "arreglo": {$elemMatch: {"campo1": "valor", "campo2": "valor"}}}, {$inc: {"arreglo.$.campo": incremento}})` | Incrementa el valor de un campo por el número especificado.
`$replaceOne` | `db.collectionName.replaceOne({idDocumentoARemplazar: "valor}, {idNuevoDocumento: "valor", "campo1": "valor", "campo2": "valor", "campo3": "valor"})` | Remplaza un documento por un nuevo documento, actualiza todos los campos.
 
**\*** Si se agrega un arreglo que no existe en el documento entonces se crea un array con ese nombre.

#### DELETE <a name="DELETE"/> [↑](#inicio)
Comando | Descripción
--------|------------
`db.collectionName.remove({})` | Elimina todos los documentos de la colección.
`db.collectionName.remove({"_idDocumento": "valor"})` | Elimina el documento indicado en el comando.
`db.collectionName.remove({"campo": "valor"}, true)` | Elimina el primer documento que conicide con el valor indicado en el campo.
`db.collectionName.remove({"campo": "valor"})` | Elimina los documentos que coniciden con el valor indicado en el campo.
`db.collectionName.delateOne({"campo": "valor"})` | Elimina el primer documento que conicide con el valor indicado en el campo.
`db.collectionName.delateMany({"campo": "valor"})` | Elimina los documentos que coniciden con el valor indicado en el campo.
`db.collectionName.deleteMany({})` | Elimina todos los documentos de la colección.

